import React, { Component } from 'react';
import { View, Text,StyleSheet,ScrollView,ImageBackground,Image } from 'react-native';
import {DrawerNavigatorItems} from 'react-navigation-drawer'
import {Ionicons} from '@expo/vector-icons'

export default class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <ScrollView>
                <ImageBackground source={require("../assets/images/menu.png")}
                 style={{width:undefined,padding:16 , paddingTop:48}}>
                    <Image source={require("../assets/images/profile_avatar.png")} style={styles.profile}/>
                    <Text style={styles.name_t}>Minh Hao </Text>
                    <View style={{flexDirection:"row"}}>
                        <Text style={styles.followers}>1000 Followers</Text>
                        <Ionicons name="md-people" size={16} color="rgba(255,255,255,0.8)"/>
                    </View>
                </ImageBackground>
                <View style={styles.container}>
                    <DrawerNavigatorItems {...this.props}/>
                </View>
            </ScrollView>
        );
    }
}
const styles =StyleSheet.create({
    container:{
        flex:1,
        fontSize:20
    },
    profile:{
        width:80,
        height:80,
        borderRadius:40,
        borderWidth:3,
        borderColor:"#FFF"
    },
    name_t:{
        color:"#FFF",
        fontSize:20,
        fontWeight:"800",
        marginVertical:8,
    },
    followers:{
        marginRight:5,
        fontSize: 13,
        color: "rgba(255,255,255,0.8)"
    }
});
