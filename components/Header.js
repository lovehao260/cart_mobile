import React from "react";
import {StyleSheet, Image, AsyncStorage, View} from "react-native";
import {Block, Text, Button} from "../constants";
import Icon from 'react-native-vector-icons/FontAwesome';
import {Input} from 'react-native-elements';

import theme from "../constants/theme";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import {checkLogin, editCart, logOut, removeCart} from "../actions";
import {connect} from "react-redux";

class Header extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            displayName: null,
            photoURL: null,

        };
    }

    render() {
        const {navigation} = this.props;
        const {displayName, photoURL} = this.state;
        return (
            <Block row padding={[0, theme.sizes.base]} style={styles.container}>
                <View style={{flex: 1}}>
                    <Input
                        onPress={() => console.log("Pressed")}
                        placeholder='search key'
                        leftIcon={
                            <Icon
                                name='search'
                                size={18}
                                color='black'
                            />
                        }
                    />
                </View>
                {displayName !== null && photoURL !== null ? (
                    <Button row middle onPress={() => navigation.navigate("Account")}>
                        <Text
                            size={theme.sizes.font}
                            style={styles.name}
                        >
                            {displayName}
                        </Text>
                        <Image source={{uri: photoURL}} style={styles.avatar}/>
                    </Button>
                ) : (
                    <Button row middle onPress={() => {
                        this.props.onLogOut()
                    }}>
                        <Text
                            size={theme.sizes.font}
                            style={styles.name}
                        >
                            Sign Out
                        </Text>
                        <Icon name="user" size={20} color="gray"/>
                    </Button>
                )}
            </Block>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: "space-between",
        alignItems: "center"
    },
    img: {
        height: 35,
        width: 125,
        resizeMode: "contain",
        justifyContent: 'center',
        alignItems: 'center'
    },
    name: {
        marginRight: 10,
        color: "gray"
    },
    avatar: {
        width: 20,
        height: 20,
        borderRadius: 10
    }
});
const mapStateToForm = state => {

    return {
        token_login: state.profile.token_login,
    }
};
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLogOut: () => {
            dispatch(logOut())
        },
    }
};
export default connect(mapStateToForm, mapDispatchToProps)(Header);
