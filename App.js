import React from 'react';
import { Provider } from "react-redux";
import configureStore from "./redux";
import Index from './index';
import * as Font from "expo-font";
import Ionicons from "react-native-vector-icons/Ionicons";
import {AppLoading} from "expo";
const store = configureStore();


 class App extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            loading: true,

        };
    }
    async UNSAFE_componentWillMount() {
        await Font.loadAsync({
            'Roboto': require('./assets/fonts/Roboto.ttf'),
            'Roboto_medium': require('./assets/fonts/Roboto_medium.ttf'),
            'HelveticaNeue':require('./assets/fonts/Krub-SemiBold.ttf'),
            ...Ionicons.font,
        });
        this.setState({ loading: false });
    }
    render() {

        if (this.state.loading) {
            return <AppLoading />;
        }
        return (
            <Provider store={store}>

                    <Index />

            </Provider>
        );

    }
}



export default App
