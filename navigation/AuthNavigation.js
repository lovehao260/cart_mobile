import {  createAppContainer } from 'react-navigation';

import  LoginScreen  from '../Screens/ProfileScreen/LoginScreen';
import  RegisterScreen  from '../Screens/ProfileScreen/RegisterScreen';
import { createStackNavigator } from 'react-navigation-stack';

import React from "react";


const AppStackNavigator = createStackNavigator(
    {
        LoginScreen,
        RegisterScreen
    },
    {
        initialRouteName: 'LoginScreen',
        headerMode: 'none',
        navigationOptions: {
            headerVisible: false,
        }
    }
);

export const AppNavigationContainer = createAppContainer(AppStackNavigator);
