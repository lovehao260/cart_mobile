import React from 'react';
import {View, Text, Dimensions} from 'react-native';
import HomeScreen from '../Screens/HomeScreen/HomeScreen'
import ProductScreen from '../Screens/HomeScreen/ProductScreen'
import DetailScreen from '../Screens/HomeScreen/DetailScreen'
import ProfileScreen from '../Screens/ProfileScreen/ProfileScreen'
import ActivityScreen from '../Screens/ActivityScreen'
import ListScreen from '../Screens/ListScreen'
import ChatScreen from '../Screens/ChatScreen/ChatScreen'
import ChatFormScreen from '../Screens/ChatScreen/ChatFormScreen'
import CartScreen from '../Screens/Cart/CartScreen'

import {createBottomTabNavigator} from "react-navigation-tabs";
import {Feather, FontAwesome5} from "@expo/vector-icons";
import {createDrawerNavigator} from 'react-navigation-drawer';
import SideBar from "../components/SideBar";
// đầu tiên tạo một context

import { createStackNavigator } from 'react-navigation-stack';
import Header from "../components/Header";
const ChatNavigator = createStackNavigator({
    ChatScreen,
    ChatFormScreen,
});


const MenuMain = createStackNavigator({
    HomeScreen,
    DetailScreen,
    ProductScreen,
});
MenuMain.navigationOptions = {
    title: "Home",
    drawerIcon: ({tintColor}) => <Feather name="home" size={20} color={tintColor}/>
};

const TabNavigator = createBottomTabNavigator({
    HomeScreen: {
        screen: MenuMain,
        navigationOptions: {
            title: "Home",
            tabBarIcon: ({tintColor}) => <FontAwesome5 name="home" size={20} color={tintColor}/>
        }
    },
    ProfileScreen: {
        screen: ProfileScreen,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => <FontAwesome5 name="user" size={20} color={tintColor}/>
        }
    },
    CartScreen: {
        screen: CartScreen,

        navigationOptions: {
            tabBarIcon: ({tintColor}) => <FontAwesome5 name="cart-plus" size={20} color={tintColor}/>
        }
    },

    ChatScreen: {
        screen: ChatNavigator,
        navigationOptions: {
            title: "Report",
            tabBarIcon: ({tintColor}) => <Feather name="message-square" size={20} color={tintColor}/>
        }
    },
    ListScreen: {
        screen: ListScreen,
        navigationOptions: {
            title: "List",
            tabBarIcon: ({tintColor}) => <FontAwesome5 name="list" size={20} color={tintColor}/>
        }
    },
}, {
    tabBarOptions: {
        showLabel: false
    },
});

export default TabNavigator
