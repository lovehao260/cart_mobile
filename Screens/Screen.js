import React from 'react'
import {View,Text,Image,TouchableOpacity,SafeAreaView,StyleSheet} from 'react-native'
import {FontAwesome5} from '@expo/vector-icons'
import {StatusBar} from "react-native-web";
export default class  Screen extends  React.Component{

    render() {
        return (
            <View style={styles.container}>
                <SafeAreaView style={{flex: 1}}>
                    <TouchableOpacity style={{ alignItems:"flex-start", margin:16}}
                                      onPress={() => {this.props.navigation.openDrawer();
                    }}
                  >
                        <FontAwesome5 name="bars" size={24} color="#161924" />
                    </TouchableOpacity>
                    <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
                        <Text style={styles.text}>
                            {this.props.name} Screen
                        </Text>
                    </View>
                </SafeAreaView>
                <StatusBar barStyle="light-content"/>
            </View>
        )
    }
}
const styles =StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"#fff",
    },
    text:{
        color:"#161924",
        fontSize:17,
        fontWeight:"500"
    }
});
