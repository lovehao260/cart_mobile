import React from "react";
import {Modal, StyleSheet, View, ScrollView, Image, TextInput, KeyboardAvoidingView, SafeAreaView} from 'react-native';
import {
    Container,
    Footer,
    Title,
    Left,
    Icon,
    Right,
    Button,
    Body,
    Content,
    Text,
    Card,
    CardItem,
    Header
} from "native-base";
import {FontAwesome5} from "@expo/vector-icons";
import {Input, SearchBar} from "react-native-elements";
import {color} from "react-native-reanimated";
import TextInputStylePropTypes from "react-native-web/dist/exports/TextInput/TextInputStylePropTypes";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";

export default class HomeScreen extends React.Component {
    state = {
        search: '',
        load: false
    };
    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: "HEADER",
            headerStyle: {backgroundColor: "#fff", height: 60},
            headerTintColor: "gray",
            headerBackTitleStyle: {display: "none"}
        };
    };


    render() {
        const {search} = this.state;
        return (
            <KeyboardAvoidingView behavior="padding" style={{flex: 1}}>
                <ScrollView style={{flex: 1}} showsHorizontalScrollIndicator={false}>
                    <View style={styles.listLove}>
                        <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                        <View style={{flexDirection: 'column'}}>
                            <View style={styles.messenger}>
                                <Text>tình hình này cũng phải lo à</Text>
                            </View>
                            <View style={styles.messenger}>
                                <Text>tình hình này cũng phải lo à,tình hình này cũng phải lo à</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.youMessenger}>
                        <View style={styles.itemYou}>
                            <Text>Ok , ban</Text>
                        </View>
                        <View style={styles.itemYou}>
                            <Text>tình hình này cũng phải lo à</Text>
                        </View>
                    </View>
                    <View style={styles.listLove}>
                        <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                        <View style={{flexDirection: 'column'}}>
                            <View style={styles.messenger}>
                                <Text>tình hình này cũng phải lo à</Text>
                            </View>
                            <View style={styles.messenger}>
                                <Text>tình hình này cũng phải lo à,tình hình này cũng phải lo à</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.listLove}>
                        <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                        <View style={{flexDirection: 'column'}}>
                            <View style={styles.messenger}>
                                <Text>tình hình này cũng phải lo à</Text>
                            </View>
                            <View style={styles.messenger}>
                                <Text>tình hình này cũng phải lo à,tình hình này cũng phải lo à</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.youMessenger}>
                        <View style={styles.itemYou}>
                            <Text>Ok , ban</Text>
                        </View>
                        <View style={styles.itemYou}>
                            <Text>tình hình này cũng phải lo à</Text>
                        </View>
                    </View>
                    <View style={styles.listLove}>
                        <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                        <View style={{flexDirection: 'column'}}>
                            <View style={styles.messenger}>
                                <Text>tình hình này cũng phải lo à</Text>
                            </View>
                            <View style={styles.messenger}>
                                <Text>tình hình này cũng phải lo à,tình hình này cũng phải lo à</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.listLove}>
                        <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                        <View style={{flexDirection: 'column'}}>
                            <View style={styles.messenger}>
                                <Text>tình hình này cũng phải lo à</Text>
                            </View>
                            <View style={styles.messenger}>
                                <Text>tình hình này cũng phải lo à,tình hình này cũng phải lo à</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.youMessenger}>
                        <View style={styles.itemYou}>
                            <Text>Ok , ban</Text>
                        </View>
                        <View style={styles.itemYou}>
                            <Text>tình hình này cũng phải lo à</Text>
                        </View>
                    </View>
                    <View style={styles.listLove}>
                        <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                        <View style={{flexDirection: 'column'}}>
                            <View style={styles.messenger}>
                                <Text>tình hình này cũng phải lo à</Text>
                            </View>
                            <View style={styles.messenger}>
                                <Text>tình hình này cũng phải lo à,tình hình này cũng phải lo à</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <TextInput
                    style={styles.inputMessenger}
                    placeholder="Ab..."
                    value={search}
                />
            </KeyboardAvoidingView>

        );

    }
}

const styles = StyleSheet.create({
    listLove: {
        flexDirection: 'row',
        width: "70%",
        marginBottom: 6,
    },
    messenger: {
        alignSelf: 'flex-start',
        backgroundColor: '#C0C0C0',
        padding: 10,
        borderRadius: 25,
        marginLeft: 10,
        marginBottom: 2,
        flex: 1,
    }
    ,
    avatar: {
        width: 40,
        height: 40,
        borderRadius: 20,
        marginLeft: 12
    },
    listChat: {
        marginTop: 5
    }
    ,
    youMessenger: {
        marginBottom: 6,

    },
    itemYou: {
        backgroundColor: '#0099FF',
        color: '#fff',
        alignSelf: 'flex-end',
        borderRadius: 25,
        padding: 8,
        marginRight: 12,
        marginBottom: 2,
    },
    textInput: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    inputMessenger: {
        borderRadius: 20,
        backgroundColor: '#EEEEEE',
        height: 50,
        margin: 15,
        paddingLeft: 15,
        width: '80%',
        justifyContent: 'flex-end',
    },
    iconInput: {
        color: '#0099FF',
        marginTop: 24
    }


});
