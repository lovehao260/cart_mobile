import React from "react";
import {
    Modal,
    StyleSheet,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    Alert,
    FlatList,
    SafeAreaView
} from 'react-native';
import {Container, Header, Title, Left, Icon, Right, Button, Body, Content, Text, Card, CardItem} from "native-base";
import {FontAwesome5} from "@expo/vector-icons";
import {SearchBar} from "react-native-elements";
import axios from 'axios';
import {editAvatar} from "../../actions";
import {connectActionSheet} from "@expo/react-native-action-sheet";
import {connect} from "react-redux";
import ProductScreen from "../HomeScreen/ProductScreen";

class ChatScreen extends React.Component {
    state = {
        search: '',
        load: false,
        users: {},
        users_id: '',
    };

    UNSAFE_componentWillMount() {
        this.loadUserChat();
    }

    loadUserChat() {
        let user = 'http://192.168.1.10/vue/public/api/user-chat/' + this.props.user.id;
        axios.get(user,
        )
            .then(res => {
                const users = res.data;
                console.log(users)
                this.setState({
                    users: users.users,
                    users_id: users.users_id,
                })

            })
            .catch(error => console.log(error));


    };

    updateSearch = search => {
        this.setState({search});
    };

    render() {
        const {search} = this.state;
        let i= 0;
        return (
            <View>
                <SearchBar
                    placeholder="Type Here..."
                    round
                    onChangeText={this.updateSearch}
                    value={search}
                    showLoading={this.state.load}
                    inputContainerStyle={{
                        backgroundColor: '#BDBDBD'
                    }}
                    containerStyle={{
                        backgroundColor: '#fff',
                        borderBottomColor: '#fff',
                        borderTopColor: '#fff',
                    }}

                />
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                    <View style={styles.listLove}>
                        <View style={styles.item}>
                            <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                            <Text>Hao</Text>
                        </View>
                        <View style={styles.item}>
                            <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                            <Text>Huy</Text>
                        </View>
                        <View style={styles.item}>
                            <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                            <Text>Huy</Text>
                        </View>
                        <View style={styles.item}>
                            <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                            <Text>Hao</Text>
                        </View>
                        <View style={styles.item}>
                            <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                            <Text>Huy</Text>
                        </View>
                        <View style={styles.item}>
                            <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                            <Text>Huy</Text>
                        </View>
                        <View style={styles.item}>
                            <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                            <Text>Hao</Text>
                        </View>
                        <View style={styles.item}>
                            <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                            <Text>Huy</Text>
                        </View>
                        <View style={styles.item}>
                            <Image source={require("../../assets/images/avatar.jpg")} style={styles.avatar}/>
                            <Text>Huy</Text>
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.listChat}>
                    <FlatList
                        data={this.state.users['1']}
                        renderItem={({item}) => (
                                <TouchableOpacity style={styles.itemFriend} onPress={() => {
                                    this.props.navigation.navigate('ChatFormScreen')
                                }}>
                                    <Image
                                        source={{uri: 'http://192.168.1.10/vue/public/storage/users-avatar/' + item.avatar}}
                                        style={styles.avatar}/>
                                    <View style={styles.friendRight}>
                                        <Text style={{fontWeight: '800'}}>{item.name}</Text>
                                        <Text style={{fontSize: 12}}>{item.message} .14h18</Text>
                                    </View>
                                </TouchableOpacity>
                        )}
                        keyExtractor={item => `${item.id}`}
                        stickyHeaderIndices={this.state.stickyHeaderIndices}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    listLove: {
        flexDirection: 'row',
        justifyContent: 'center',
        textAlign: 'center'
    },
    item: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
    ,
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginLeft: 12
    },
    listChat: {
        marginTop: 5
    }
    ,
    itemFriend: {
        flexDirection: 'row',
        marginTop: 8
    },
    friendRight: {
        marginLeft: 15
    }
});
const mapStateToForm = state => {
    return {
        user: state.profile.user,
        token: state.profile.token_login
    }
};
const mapDispatchToProps = (dispatch, props) => {
    return {
        onEditAvatar: (data) => {
            dispatch(editAvatar(data))
        },
    }
};
export default connect(mapStateToForm, mapDispatchToProps)(ChatScreen);
