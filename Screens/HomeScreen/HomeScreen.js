import React from "react";
import {StatusBar, StyleSheet, ScrollView, FlatList, View, SafeAreaView, AsyncStorage} from "react-native";
import {Container, Title, Left, Icon, Right, Button, Body, Content, Text, Card, CardItem} from "native-base";
import {SearchBar} from 'react-native-elements';
import Constants from 'expo-constants';
import Header from '../../components/Header';
import ProductScreen from './ProductScreen'
const AppContext = React.createContext();

export default class HomeScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: (
                <Header navigation={navigation} />
            ),
            headerStyle: { backgroundColor: "#fff", height: 60 },
            headerTintColor: "gray",
            headerBackTitleStyle: { display: "none" }
        };
    };


    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: [
                {id: 1, name: "IphoneX", price:"25000",img:'https://cdn.tgdd.vn/Products/Images/42/200533/iphone-11-pro-max-black-400x460.png'},
                {id: 2, name: "Iphone 11", price:"55000",img:'https://cdn.tgdd.vn/Products/Images/42/200533/iphone-11-pro-max-black-400x460.png'},
                {id: 3, name: "Iphone Pro Max", price:"35000",img:'https://cdn.tgdd.vn/Products/Images/42/200533/iphone-11-pro-max-black-400x460.png'},
                {id: 4, name: "Samsung Note 10", price:"65000",img:'https://cdn.tgdd.vn/Products/Images/42/211570/samsung-galaxy-a51-white-400x460.png'},
                {id: 5, name: "Galaxy S10 ", price:"85000",img:"https://cdn.tgdd.vn/Products/Images/42/211570/samsung-galaxy-a51-white-400x460.png"}
            ],
            error: null,
            search: ''
        };
    }

    updateSearch = search => {
        this.setState({search});
    };

    render() {
        const {data, search} = this.state;
        return (

            <Container>
                <SafeAreaView style={{flex: 1}}>
                    <FlatList
                        data={data} numColumns={2}
                        renderItem={({item}) => (
                            <View style={styles.wrapper}>
                                <ProductScreen navigation={this.props.navigation} category={item}/>
                            </View>
                        )}
                        keyExtractor={item => `${item.id}`}
                        stickyHeaderIndices={this.state.stickyHeaderIndices}
                    />
                </SafeAreaView>
            </Container>
        );
    }
}
const styles=StyleSheet.create({
    wrapper:{
        flex:1,
        paddingHorizontal:6

    }
});
