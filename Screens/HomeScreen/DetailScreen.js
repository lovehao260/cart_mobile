import React, {Component} from 'react';
import {Text, View, Image, StyleSheet, TouchableOpacity, Alert, Button, AsyncStorage} from 'react-native';
import {connect} from "react-redux";
import {addCart} from '../../actions/index'

 class DetailScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cartItems: []
        };
    };
    static navigationOptions = ({navigation}) => {
        const data = (navigation.getParam('productName'));
        return {
            headerTitle: data.name,
            headerStyle: {backgroundColor: "#fff", height: 60},
            headerTintColor: "gray",
            headerBackTitleStyle: {display: "none"}
        };
    };

    render() {
        const data = (this.props.navigation.getParam('productName'));
        const {navigation}=this.props;

        return (

            <View style={styles.container}>
                <Image style={styles.productImage} source={{uri: data.img}}/>
                <Text>{data.price} $</Text>
                <View style={styles.contact}>
                    <Button
                        title="Add To Card"
                        flex="1"
                        onPress={() => {
                            this.props.onSaveTask(data);
                            navigation.navigate('CartScreen',{
                                productName: this.props.category,
                            })
                        }}
                    />
                    <Text style={styles.contact}>
                        Adjust the color in a way that looks standard on each platform. On
                        iOS, the color prop controls the color of the text. On Android, the
                        color adjusts the backgroud color of the button.
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 16,
        shadowColor: '#000',
        borderRadius: 4,
        backgroundColor: '#fff',
        shadowOpacity: 0.1,
        marginBottom: 15,
        shadowRadius: 10,
        alignSelf: 'stretch',
        flex: 1
    },
    productImage: {
        marginTop: 5,
        width: 164,
        height: 164,
        resizeMode: 'contain'
    },
    contact: {
        flex: 1,
        justifyContent: "center",
        textAlign: 'center',
        alignSelf: 'stretch',
    }

});
const mapStateToForm = state=>{
    return {

    }
};
const mapDispatchToProps =(dispatch,props)=>{
    return {
        onSaveTask:(task)=>{
            dispatch(addCart(task))
        },

    }
};
export default connect(mapStateToForm,mapDispatchToProps)(DetailScreen);
