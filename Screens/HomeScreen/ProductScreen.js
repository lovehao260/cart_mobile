import React, { Component } from 'react';
import { Text, View,Image,StyleSheet,TouchableOpacity,Alert } from 'react-native';
import TestImage from '../../assets/images/iphone11.jpg'

export default class ProductScreen extends Component {
    constructor(props){
        super(props);
        this.state={
        }
    }
    render() {
        const {navigation}=this.props;
        return (
            <TouchableOpacity activeOpacity={0.4} onPress={()=>{
                navigation.navigate('DetailScreen',{
                    productName: this.props.category,
                })
            }}>
                <View style={styles.container}>
                    <Text style={styles.title}>{this.props.category.name}</Text>
                    <Image style={styles.categoryImage} source={{uri:this.props.category.img}}/>
                    <Text style={styles.title}>{this.props.category.price} VND</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        alignItems:'center',
        padding:10,
        shadowColor:'#000',
        borderRadius:4,
        backgroundColor:'#fff',
        shadowOpacity:0.1,
        marginBottom:15,
        shadowRadius:10,
        elevation: 10,
        alignSelf: 'stretch',

    },

    categoryImage: {
        marginTop:5,
        width:64,
        height:64,
        resizeMode: 'contain'
    },
    red: {
        color: 'red',
    },
});
