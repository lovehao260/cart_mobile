import React from "react";
import {SafeAreaView, View, TouchableOpacity, FlatList, StyleSheet, Image, Alert} from "react-native";
import {
    Container,
    Header,
    Footer,
    Title,
    Left,
    Icon,
    Right,
    Button,
    Body,
    Content,
    Text,
    Card,
    CardItem
} from "native-base";
import {FontAwesome5} from "@expo/vector-icons";

import {connect} from "react-redux";
import {removeCart, editCart} from "../../actions";
import cart from "../../reducers/cartReducers";
import ProductScreen from "../HomeScreen/ProductScreen";

class Cart extends React.Component {
    static navigationOptions = ({navigation}) => {

        return {
            headerTitle: "CART",
            headerStyle: {backgroundColor: "#fff", height: 60},
            headerTintColor: "gray",
            headerBackTitleStyle: {display: "none"}
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            total: 0
        };
    }

    render() {
        let data = this.props.carts;
        let total = 0;
        data.forEach((item) => {
            total += item.product.price * item.quantity;
            return total
        });

        if (data.length !== 0) {
            return (

                <Container>
                    <Header>
                    </Header>
                    <SafeAreaView style={{flex: 1}}>
                        <FlatList data={data}
                                  renderItem={({item, index}) => (
                                      <View style={styles.container}>
                                          <View style={{flex: 2}}>

                                              <Image style={styles.categoryImage} source={{uri: item.product.img}}/>
                                              <Text>{item.product.name}</Text>
                                          </View>
                                          <View style={{flex: 1}}>
                                              <View style={{flexDirection: "row"}}>
                                                  <Icon name="ios-remove-circle-outline" onPress={
                                                      () => {
                                                          this.props.onUpdateCart(item.product.id, item.quantity, "sub");
                                                      }
                                                  }/>
                                                  <Text style={{padding: 5}}>{item.quantity}</Text>
                                                  <Icon name="ios-add-circle-outline" onPress={
                                                      () => {
                                                          this.props.onUpdateCart(item.product.id, item.quantity, "add");
                                                      }
                                                  }/>
                                              </View>
                                          </View>
                                          <View style={{flex: 2}}>
                                              <View style={{flexDirection: "row"}}>
                                                  <Text
                                                      style={{paddingLeft: 25}}>{item.quantity * item.product.price} $</Text>
                                              </View>
                                          </View>
                                          <View style={{flex: 1}}>
                                              <Icon name="md-trash" onPress={() =>
                                                  Alert.alert(
                                                      'Confirm',
                                                      'Remove Product From Cart',
                                                      [
                                                          {
                                                              text: 'Cancel',
                                                              style: 'cancel',
                                                          },
                                                          {
                                                              text: 'OK', onPress: () => {
                                                                  this.props.onRemove(item.product.id);
                                                              }
                                                          },
                                                      ],
                                                      {cancelable: false},
                                                  )
                                              }/>
                                          </View>
                                      </View>
                                  )}
                                  keyExtractor={(item, index) => `${index}`}
                        />
                    </SafeAreaView>
                    <Footer>
                        <View style={styles.container_bottom}>
                            <View>
                                <Text>Total Price:</Text>
                            </View>
                            <View>
                                <Text>{total} $</Text>
                            </View>
                        </View>
                        <View>

                        </View>
                    </Footer>
                </Container>
            );
        } else {
            return (
                <Text>
                    No product in your cart
                </Text>
            )
        }

    }
}

const mapStateToForm = state => {
    return {
        carts: state.cart.carts,
        total: state.cart.total
    }
};
const mapDispatchToProps = (dispatch, props) => {
    return {
        onRemove: (data) => {
            dispatch(removeCart(data))
        },
        onUpdateCart: (id, number, type) => {
            dispatch(editCart(id, number, type))
        },

    }
};
const styles = StyleSheet.create({
    container: {

        alignItems: 'center',
        shadowColor: '#000',
        borderRadius: 4,
        backgroundColor: '#fff',
        shadowOpacity: 0.1,
        marginBottom: 15,
        shadowRadius: 10,
        elevation: 10,
        justifyContent: "space-between",
        flexDirection: 'row'
    },
    container_bottom: {

        alignItems: 'center',
        shadowColor: '#000',
        borderRadius: 4,
        backgroundColor: '#fff',
        shadowOpacity: 0.1,
        marginBottom: 15,
        shadowRadius: 10,
        paddingHorizontal: 10,
        elevation: 10,
        flexDirection: 'row',
        width: '100%',
        height: 60,
        justifyContent: "space-between",

    },
    categoryImage: {
        marginLeft: 20,
        marginTop: 5,
        width: 44,
        height: 64,
        resizeMode: 'contain'
    },
    red: {
        color: 'red',
    },
    item: {
        width: '30%' // is 50% of container width
    }
});


export default connect(mapStateToForm, mapDispatchToProps)(Cart);
