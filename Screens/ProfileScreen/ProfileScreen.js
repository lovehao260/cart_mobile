import React from "react";
import { Container, Header, Title, Left, Icon, Right, Body, Content, Card, CardItem } from "native-base";
import {connectActionSheet} from '@expo/react-native-action-sheet'
import * as MediaLibrary from 'expo-media-library';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import axios from 'axios';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    SafeAreaView,
    StyleSheet,
    ScrollView,
    Button,
    AsyncStorage, ActionSheetIOS, Alert, Modal, TouchableHighlight
} from 'react-native'
import {MaterialIcons, Ionicons} from '@expo/vector-icons'

import {connect} from "react-redux";
import {addCart, editAvatar} from "../../actions";


class ProfileScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            photos: null,
            file: null,
            modalVisible: false,
        };
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    componentDidMount() {
        this.getPermissionAsync();

    }

    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
            base64: true
        });
        if (!result.cancelled) {
            this.setState({photos: result});
            this.onDoUploadPress(result.base64)

        }
    };

    _openCamera = async () => {

        let result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
            base64: true
        });
        if (!result.cancelled) {
            this.setState({photos: result});
            this.onDoUploadPress(result.base64)
        }
    };
    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);

            if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
            }
        }
    };


    _onOpenActionSheet = () => {

        const options = ['View Profile Picture', 'Select Profile Picture', 'Open Camera', 'Cancel'];
        const cancelButtonIndex = 3;

        this.props.showActionSheetWithOptions(
            {
                options,
                cancelButtonIndex,

                containerStyle: {
                    backgroundColor: '#fff'
                },
                tintColor: '#3399FF',
                message: 'Profile Picture',
            },
            buttonIndex => {
                switch (buttonIndex) {
                    case 0:
                        this.setModalVisible(true);
                        break;
                    case 1:
                        this._pickImage()
                        break;
                    case 2:
                        this._openCamera()
                        break;
                    default:
                    // code block
                }
            },
        );
    };

    async onDoUploadPress(file) {
        let id = this.props.user.id;
        axios.post('http://192.168.1.10/vue/public/api/uploadAvatar/' + id, {"avatar": file},
            // {
            //     headers:{
            //         'Content-Type' : 'multipart/form-data',
            //     }
            // }
        )
            .then(res => {
                const persons = res.data;
                this.props.onEditAvatar(persons)
            })
            .catch(error => console.log(error));

    }

    UNSAFE_componentWillMount() {
        AsyncStorage.getItem('LoginToken').then(res => {
            this.setState({token: res});
        });
    }

    render() {
        let data = this.props.user;

        let imgNew = <Image
            source={this.state.photos === null ? {uri: 'http://192.168.1.10/vue/public/storage/users-avatar/' + data.avatar} : this.state.photos}
            style={styles.image}/>

        return (
            <SafeAreaView style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Image source={require("../../assets/images/bacgroundHeader.jpg")} style={styles.banner}/>
                    <TouchableOpacity onPress={() => {
                        this._onOpenActionSheet()
                    }} style={{alignSelf: 'center', marginTop: 150,}}>

                        <View style={styles.profileImage}>
                            <View onPress={() =>
                                this._onOpenActionSheet()
                            }>
                                {imgNew}
                            </View>

                        </View>
                        <View style={styles.add}>
                            <Ionicons name='ios-camera' size={20} color="#fff">
                            </Ionicons>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.infoContainer}>
                        <Text style={[styles.text, {fontWeight: '200', fontSize: 24}]}>{data.name}</Text>
                        <Text style={[styles.text, {color: '#AEB5BC', fontSize: 14}]}>Developer</Text>
                    </View>
                    <View style={{
                        borderBottomColor: '#AEB5BC',
                        borderBottomWidth: 1,
                    }}/>
                    <View style={styles.statsContainer}>
                        <View style={styles.about}>
                            <Ionicons name='ios-mail' size={20} color="#AEB5BC"/>
                            <Text style={styles.aboutItem}>
                                Email <Text style={{fontWeight: '500'}}>{data.email} </Text>
                            </Text>
                        </View>
                        <View style={styles.about}>
                            <Ionicons name='ios-home' size={20} color="#AEB5BC"/>
                            <Text style={styles.aboutItem}>
                                {data.email}
                            </Text>
                        </View>
                        <View style={styles.about}>
                            <Ionicons name='ios-mail' size={20} color="#AEB5BC"/>
                            <Text style={styles.aboutItem}>
                                {data.email}
                            </Text>
                        </View>
                        <View style={styles.about}>
                            <Ionicons name='ios-time' size={20} color="#AEB5BC"/>
                            <Text style={styles.aboutItem}>
                                Joined {data.created_at}
                            </Text>
                        </View>
                    </View>
                    <TouchableOpacity>
                        <Button title='Edit Profile'>

                        </Button>
                    </TouchableOpacity>
                </ScrollView>
                <View>
                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            Alert.alert('Modal has been closed.');
                        }}>
                        <TouchableHighlight
                            onPress={() => {
                                this.setModalVisible(!this.state.modalVisible);
                            }} style={styles.close} >
                            <Ionicons name='ios-close-circle-outline' size={40} color="#fff">
                            </Ionicons>
                        </TouchableHighlight>
                        <View style={styles.modalAvatar}>

                            <View>
                                <Image
                                    source={this.state.photos === null ? {uri: 'http://192.168.1.10/vue/public/storage/users-avatar/' + data.avatar} : this.state.photos}
                                    style={styles.viewAvatar}/>

                            </View>
                        </View>

                    </Modal>
                </View>
            </SafeAreaView>

        );
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 10
    },
    text: {
        fontFamily: "HelveticaNeue",
        color: '#52575D'
    },
    banner: {
        position: 'absolute',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        top: '2%',
        height: 200,

    }
    ,
    profileImage: {
        borderColor: '#606070',
        shadowOpacity: 0.4,
        borderRadius: 15,
    },

    image: {
        width: 150,
        height: 150,
        borderRadius: 150 / 2,
        overflow: "hidden",
        borderWidth: 3,
        borderColor: "white"
    },
    active: {
        backgroundColor: "#34FFB9",
        position: 'absolute',
        bottom: 28,
        left: 10,
        padding: 4,
        height: 20,
        width: 20,
        borderRadius: 20,

    },
    add: {
        backgroundColor: '#41444B',
        position: 'absolute',
        bottom: 10,
        right: 0,
        height: 30,
        width: 30,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',

        overflow: "hidden",
        borderWidth: 2,
        borderColor: "white"
    },
    infoContainer: {
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 16,

    },
    statsContainer: {
        marginTop: 20,

    },
    about: {
        flexDirection: 'row',
        fontSize: 20,
        marginBottom: 15
    },
    aboutItem: {
        fontSize: 18,
        paddingLeft: 15
    },
    modalAvatar: {
        flex: 1,
        justifyContent:'center',
        backgroundColor: 'rgba(0,0,0,0.6)',
    },
    viewAvatar:{
        flexDirection:'row',
        width: '100%',
        height: 400,
        zIndex:2,
    },
    close:{
      position:'absolute',
        top: 30,
        zIndex:2,
    }


});

const mapStateToForm = state => {
    return {
        user: state.profile.user,
        token: state.profile.token_login
    }
};
const mapDispatchToProps = (dispatch, props) => {
    return {
        onEditAvatar: (data) => {
            dispatch(editAvatar(data))
        },
    }
};
const ConnectedApp = connectActionSheet(ProfileScreen);
export default connect(mapStateToForm, mapDispatchToProps)(ConnectedApp);
