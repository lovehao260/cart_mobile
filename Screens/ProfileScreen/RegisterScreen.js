import React, {Component} from "react";


import {
    Keyboard,
    Text,
    View,
    TextInput,
    TouchableWithoutFeedback,
    Alert,
    KeyboardAvoidingView,
    Image
} from 'react-native';
import {Button, Divider} from 'react-native-elements';

import styles from "../../assets/style/login";
export default class LoginScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            nameU: '',
            email: '',
            password: '',
            c_password: '',
        }
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.loginScreenContainer}>
                    <Image source={require('../../assets/images/login.png')} style={styles.header}/>
                    <Image source={require('../../assets/images/apple.png')} style={styles.logo}/>
                    <View style={styles.loginFormView}>

                        <TextInput placeholder="Name " placeholderColor="#c4c3cb"
                                   style={styles.loginFormTextInput}
                                   onChangeText={(nameU) => this.setState({nameU})}
                        />
                        <TextInput placeholder="Email" placeholderColor="#c4c3cb"
                                   autoCompleteType="email"
                                   style={styles.loginFormTextInput}
                                   onChangeText={(email) => this.setState({email})}
                                   value={this.state.email}/>
                        <TextInput placeholder="Password" placeholderColor="#c4c3cb"
                                   style={styles.loginFormTextInput} secureTextEntry={true}
                                   value={this.state.password}
                                   onChangeText={(password) => this.setState({password})}/>
                        <TextInput placeholder="Password" placeholderColor="#c4c3cb"
                                   style={styles.loginFormTextInput} secureTextEntry={true}
                                   value={this.state.c_password}
                                   onChangeText={(c_password) => this.setState({c_password})}/>
                        <Button
                            buttonStyle={styles.loginButton}
                            onPress={() =>  this.onRegister()}
                            title="Register"
                        />
                        <View style={{alignSelf: 'center', marginTop: 32}}>
                            <Text>
                                You to SocialApp ?
                                <Text style={{fontWeight: '500', color: "#E9446A"}}
                                      onPress={() => {
                                          this.props.navigation.navigate('LoginScreen')
                                      }}> Sign In</Text>
                            </Text>
                        </View>
                    </View>

                </View>

            </TouchableWithoutFeedback>
        );

    }

    async onRegister() {

        let name = this.state.nameU;
        let email = this.state.email;
        let password = this.state.password;
        let c_password = this.state.c_password;

        await fetch('http://192.168.1.10/vue/public/api/register?', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "name": name,
                "email": email,
                "password": password,
                "c_password": c_password
            })
        }).then(res => res.json())
            .then(resData => {
                if(resData.hasOwnProperty('error')){
                    Alert.alert(
                        'Incorrect Password',
                        'The password you entered is incorrect .Please try again',
                    )
                }
                else{
                    this.props.navigation.navigate('LoginScreen')
                }
            })
    };


}
