import React, {Component} from "react";


import {
    Keyboard,
    Text,
    View,
    Image,
    TextInput,
    TouchableWithoutFeedback,
    Alert,
    KeyboardAvoidingView,
    TouchableOpacity,
    AsyncStorage
} from 'react-native';
import {Button, Divider} from 'react-native-elements';

import styles from "../../assets/style/login";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import {checkLogin} from "../../actions";
import {connect} from "react-redux";

class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
        }
    }

    render() {

        return (

            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

                <View style={styles.loginScreenContainer}>
                    <Image source={require('../../assets/images/login.png')} style={styles.header}/>
                    <Image source={require('../../assets/images/apple.png')} style={styles.logo}/>
                    <View style={styles.loginFormView}>

                        <TextInput placeholder="Username" placeholderColor="#c4c3cb"
                                   style={styles.loginFormTextInput}
                                   onChangeText={(email) => this.setState({email})}
                                   value={this.state.email}/>
                        <TextInput placeholder="Password" placeholderColor="#c4c3cb"
                                   style={styles.loginFormTextInput} secureTextEntry={true}
                                   value={this.state.password}
                                   onChangeText={(password) => this.setState({password})}
                        />
                        <TouchableOpacity>
                            <Button
                                buttonStyle={styles.loginButton}
                                onPress={() => this.onLoginPress()}
                                title="Sign in"
                            />
                        </TouchableOpacity>
                        <View style={{alignSelf: 'center', marginTop: 32}}>
                            <Text>
                                New to SocialApp ?
                                <Text style={{fontWeight: '500', color: "#E9446A"}}
                                      onPress={() => {
                                          this.props.navigation.navigate('RegisterScreen')
                                      }}> Sign Up</Text>
                            </Text>
                        </View>
                    </View>
                </View>

            </TouchableWithoutFeedback>

        );
    }

    async onLoginPress() {
        let email = this.state.email;
        let password = this.state.password;
        await fetch('http://192.168.1.10/vue/public/api/login?', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({"email": email, "password": password})
        }).then(res => res.json())
            .then(resData => {
                if (resData.hasOwnProperty('error')) {
                    Alert.alert(
                        'Incorrect Password',
                        'The password you entered is incorrect .Please try again',
                    )
                } else {
                    this.props.onSaveToken(resData.success.token, resData.user);

                }
            })
    };


    async onFbLoginPress() {
        const {type, token} = await Expo.Facebook.logInWithReadPermissionsAsync(appId, {
            permissions: ['public_profile', 'email'],
        });
        if (type === 'success') {
            const response = await fetch(
                `https://graph.facebook.com/me?access_token=${token}`);
            Alert.alert(
                'Logged in!',
                `Hi ${(await response.json()).name}!`,
            );
        }
    }
}

const mapStateToForm = state => {
    return {}
};
const mapDispatchToProps = (dispatch, props) => {
    return {
        onSaveToken: (token, user) => {
            dispatch(checkLogin(token, user));
        },

    }
};
export default connect(mapStateToForm, mapDispatchToProps)(LoginScreen);
