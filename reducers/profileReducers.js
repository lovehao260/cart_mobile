import * as types from '../actions/actionTypes';
import _ from 'lodash'
import {AsyncStorage} from "react-native";
const initialState = {
    token_login: '',
    user:{}

};

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case types.CHECK_LOGIN: {
            const data = action.token;
             AsyncStorage.setItem('LoginToken',data);
            AsyncStorage.setItem('UserData',JSON.stringify(action.user));
            return {
                ...state,
                token_login: data,
                user:action.user
            }
        }case types.LOG_OUT:{
             AsyncStorage.removeItem('LoginToken');
            AsyncStorage.removeItem('UserData');
            return {
                ...state,
                token_login: '',
                user:{}
            }
        }
        case types.EDIT_AVATAR:{
            AsyncStorage.setItem('UserData',JSON.stringify(action.data));
            return {
                ...state,
                user:action.data
            }
        }

        default:
            return state;
    }
};

export default reducer;
