import * as types from '../actions/actionTypes';
import _ from 'lodash'
const initialState = {
    carts: [],

};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_CART: {
            const data = action.product;
            let {carts} = state;
            const cart = {
                product: data,
                quantity: 1,

            };
            const index = carts.findIndex(item => item.product.id === data.id);

            if (index === -1) {
                carts = carts.concat([cart]);
            } else {
                carts[index].quantity = carts[index].quantity + 1;
            }
            return {
                ...state,
                carts: carts,

            }
        }
        case types.REMOVE_FROM_CART:{
            let {carts} = state;

            carts = carts.filter(i => i.product.id !== action.id)

            return {
                ...state,
                carts: carts,
            }
        }
        case types.EDIT_CART:{
            let carts = _.cloneDeep(state.carts);

            const index = carts.findIndex(item => item.product.id === action.id);
            if (index !== -1) {
                if (action.text==='sub'  ){
                    if (carts[index].quantity === 1) {
                        carts = carts.filter(i => i.product.id !== action.id);
                    } else {
                     carts[index].quantity = carts[index].quantity - 1;
                    }

                }else if (action.text==='add' && carts[index].quantity < 10 ) {
                    carts[index].quantity = carts[index].quantity + 1;
                }
            }
            return {
                ...state,
                carts: carts,
            }
        }
        default:
            return state;
    }
};

export default reducer;
