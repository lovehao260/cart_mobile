import {combineReducers} from 'redux';
import cart from './cartReducers'
import profile from './profileReducers'
const  allReducers = combineReducers({
    cart,
    profile
});
export default allReducers;
