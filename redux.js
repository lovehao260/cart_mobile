import {createStore} from 'redux';

import rootReducer from './reducers';

import {composeWithDevTools} from 'remote-redux-devtools';


const configurStore = () => {
  return createStore(rootReducer, composeWithDevTools());
};


export default configurStore;
