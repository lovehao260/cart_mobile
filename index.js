import React from 'react';
import {createAppContainer} from 'react-navigation';
import AppNavigator from './navigation/AppNavigator';
const AppContainer = createAppContainer(AppNavigator);
import { AppNavigationContainer } from './navigation/AuthNavigation';
const LoginContainer = createAppContainer(AppNavigationContainer);
import {connect} from "react-redux";
import { connectActionSheet } from '@expo/react-native-action-sheet'
import { ActionSheetProvider } from '@expo/react-native-action-sheet'
import {checkLogin} from "./actions";

import {AsyncStorage} from "react-native";
 class Index extends React.Component{
     async UNSAFE_componentWillMount() {
         let token = await AsyncStorage.getItem('LoginToken');
         let user = await AsyncStorage.getItem('UserData');
         this.props.onSaveToken(token,JSON.parse(user))
     }

    render() {
        if(this.props.token_login ){
            return (
                <ActionSheetProvider>
                    <AppContainer/>
                </ActionSheetProvider>
            );
        }else{
            return (
                <LoginContainer />
            );
        }

    }
}
const mapStateToForm = state => {

    return {
        token_login: state.profile.token_login,
        user: state.profile.user,
    }
};
const mapDispatchToProps = (dispatch, props) => {
    return {
        onSaveToken: (token, user) => {
            dispatch(checkLogin(token, user));
        },
    }
};
const ConnectedApp = connectActionSheet(Index);
export default connect(mapStateToForm, mapDispatchToProps)(ConnectedApp);
