const React = require("react-native");

const {StyleSheet} = React;

export default {

    containerView: {
        flex: 1,
    },
    loginScreenContainer: {
        flex: 1,
    },
    header: {
        height: 150

    },
    logo: {
        alignSelf: 'center',
        width: 60,
        height: 60,
        marginTop: 50,
        marginBottom: 40,
    },
    logoText: {
        fontSize: 40,
        fontWeight: "800",
        marginTop: 10,
        marginBottom: 30,
        textAlign: 'center',
    },
    loginFormView: {
        flex: 1
    },
    loginFormTextInput: {
        height: 43,
        fontSize: 14,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#eaeaea',
        backgroundColor: '#fafafa',
        paddingLeft: 10,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 5,
        marginBottom: 5,

    },
    loginButton: {
        marginHorizontal: 15,
        backgroundColor: '#3897f1',
        borderRadius: 5,
        height: 45,
        marginTop: 10,
    },
    fbLoginButton: {

        backgroundColor: '#3897f1',
        height: 45,
        marginTop: 10,

    },
    buttonLogin: {
        marginHorizontal: 15,
        justifyContent: "space-between",
        flexDirection: 'row',
        width: "100%",
        height: 60,
        alignItems: 'center'
    },
    line: {
        height: 1,
        flex: 2,
        backgroundColor: 'black'
    },
    textOr: {
        flex: 1,
        textAlign: "center"
    },
    divider: {
        flexDirection: 'row',
        height: 40,
        width: 298,
        justifyContent: 'center',
        alignItems: 'center'

    },
    buttonText: {
        color: "#fff",
        fontSize: 18
    }

};
