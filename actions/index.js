import {ADD_CART, REMOVE_FROM_CART, EDIT_CART, CHECK_LOGIN, LOG_OUT, EDIT_AVATAR} from './actionTypes'

export const addCart = (product) => {
    return {
        type: ADD_CART,
        product: product,
    }
};
export const removeCart = (id) => {
    return {
        type: REMOVE_FROM_CART,
        id
    };
};

export function editCart(id, quantity, text) {

    return {
        type: EDIT_CART,
        id,
        quantity,
        text
    };
}

export function checkLogin(token,user) {
    return {
        type: CHECK_LOGIN,
        token,
        user
    };
}

export function logOut() {
    return {
        type: LOG_OUT,
    };
}

export function editAvatar(data) {

    return {
        type: EDIT_AVATAR,
        data
    };
}
