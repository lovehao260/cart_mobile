export  const CHECK_LOGIN='CHECK_LOGIN';
export  const LOG_OUT='LOG_OUT';
export  const ADD_CART='ADD_CART';
export  const REMOVE_FROM_CART='REMOVE_FROM_CART';
export  const EDIT_CART='EDIT_CART';
export  const EDIT_AVATAR='EDIT_AVATAR';
